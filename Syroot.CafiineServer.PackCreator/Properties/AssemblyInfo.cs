﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Pack Creator")]
[assembly: AssemblyDescription("Syroot Cafiine Server Pack Creator")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.CafiineServer.PackCreator")]
[assembly: AssemblyCopyright("Copyright © 2016 Syroot")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.8.0.0")]
[assembly: AssemblyFileVersion("0.8.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("54151dbe-437f-4a94-958a-1a4ee44e3df2")]
