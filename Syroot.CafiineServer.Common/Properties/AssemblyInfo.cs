﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Cafiine Server Common")]
[assembly: AssemblyDescription("Syroot Cafiine Server Common Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Syroot")]
[assembly: AssemblyProduct("Syroot.CafiineServer.Common")]
[assembly: AssemblyCopyright("Copyright © 2016 Syroot")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("0.8.0.0")]
[assembly: AssemblyFileVersion("0.8.0.0")]
[assembly: ComVisible(false)]
[assembly: Guid("92902185-a5d9-4cbf-a722-c21d09d713a2")]
